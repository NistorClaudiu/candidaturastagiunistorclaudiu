public class Triangle extends Shape{
	
	double l1, l2, l3;
	
	Triangle(double l1, double l2, double l3)
	{
		this.l1=l1;
		this.l2=l2;
		this.l3=l3;
	}
	
	public double calculateArea() {
        double p=(l1+l2+l3)/2;
		return Math.sqrt(p*(p-l1)*(p-l2)*(p-l3));
	}

	
	public double calculatePerimeter() {
            
		return l1+l2+l3;
	}
	
	

}
