public class Rectangle extends Shape {
	
	double l1,l2;

	Rectangle(double l1, double l2)
	{
		this.l1=l1;
		this.l2=l2;
	}
	

	public double calculateArea() {
             
		return l1*l2;
	}

	@Override
	public double calculatePerimeter() {
		
		return 2*l1+2*l2;
	}
	
	
}
