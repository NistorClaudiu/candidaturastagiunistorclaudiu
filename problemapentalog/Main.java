import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		Scanner sc= new Scanner(System.in);

		
		int a;
		int optiune=1;
		
		
		while(optiune==1)
			
		{
			System.out.println("==Choose Figure==\n");
			System.out.println("1) Triangle");
			System.out.println("2) Circle");
			System.out.println("3) Rectangle");
			
			a=sc.nextInt();
			switch(a){
			
			case 1:
			{
				boolean reincearca=true;
				
				
				
				while (reincearca)
					{
						System.out.println("Introduce sizes:");
							
						double l1, l2, l3;
						System.out.print("Size 1:\t");
						l1=sc.nextDouble();
						System.out.print("Size 2:\t");
						l2=sc.nextDouble();
						System.out.print("Size 3:\t");
						l3=sc.nextDouble();
						if(l1+l2>l3 && l2+l3>l1 && l1+l3>l2)	
						{
							Triangle triunghi = new Triangle(l1,l2,l3);
							System.out.println("Perimeter:\t"+triunghi.calculatePerimeter()+"\tArea:\t"+triunghi.calculateArea());
							reincearca=false;
						}
						else
						{
							System.out.println("Given lengths don't form a triangle! Retry");
							reincearca=true;
						}
					}
			}
			break;
		
			case 2:
			{
				System.out.println("Introduce radius:");
				double r;
				System.out.print("Radius:\t");
				r=sc.nextDouble();
				Circle cerc=new Circle(r);
				System.out.println("Perimeter:\t"+cerc.calculatePerimeter()+"\tArea:\t"+cerc.calculateArea());
				
			}
			break;
			
			case 3:
			{
				System.out.println("Introduce lenght and width:");
				double l1;
				double l2;
				System.out.print("Length:\t");
				l1=sc.nextDouble();
				System.out.print("Width:\t");
				l2=sc.nextDouble();
				Rectangle dreptunghi=new Rectangle(l1,l2);
				System.out.println("Perimeter:\t"+dreptunghi.calculatePerimeter()+"\tArea:\t"+dreptunghi.calculateArea());
				
				
			}
			
			default: 
				{
					System.out.println("Error!");
				}
				break;
		
			
		}
			
			
			System.out.println("Need another try?\t 1 = Yes - 0 = No");
			optiune=sc.nextInt();
		}
		
		
		
	}

}
