public class Circle extends Shape {

	double pi=3.14;
	double r;
	
	Circle(double r)
	{
		this.r=r;
	}
	

	public double calculateArea() {
             
		return pi*Math.pow(r, 2);
	}

	@Override
	public double calculatePerimeter() {
		// TODO Auto-generated method stub
		return 2*pi*r;
	}
	
	
}
