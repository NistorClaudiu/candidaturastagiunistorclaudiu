public interface Sizes {
	
	public double calculateArea();
	
	public double calculatePerimeter();

}
